#!/bin/sh
pip install ibm_db_sa;
pip install mysql-connector-python;
pip install scikit-learn --upgrade;
pip install category_encoders;
pip install imblearn;
pip install xgboost;
pip install mlxtend;
pip install matplotlib --upgrade;
pip install seaborn --upgrade;
pip install pandas-profiling;