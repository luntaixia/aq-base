import json
import requests
from urllib3.exceptions import HTTPError

func_name = "stat_sales"
params = {
    "start_dt" : "2020-05-01",
    "end_dt" : "2021-11-09"
}

response = requests.post(
    url = f"http://10.56.70.110:9999/app/{func_name}",
    json = params,
    headers = {"content-type": "application/json"}
)

if response.status_code == 200:
    body = json.loads(response.text)
else:
    err = f"Request Error Code = {response.status_code}\n" \
          + f"content-type: {response.headers['content-type']}\n\n" \
          + f"Message from server:\n" \
          + response.text
    raise HTTPError(err)


import pandas as pd

pd.DataFrame.from_records(body)