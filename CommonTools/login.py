from configparser import ConfigParser
from getpass import getpass
from CommonTools.settings import SETTINGS

def get_credential():
    if SETTINGS.DB_CREDENTIAL_INTERACTIVE:
        username = getpass(prompt = 'Your SID: ')
        password = getpass(prompt = 'Your password: ')
        return username, password
    else:
        config = ConfigParser()
        config.read(SETTINGS.DB_CREDENTIAL_PATH)
        username = config["Database"]["username"]
        password = config["Database"]["password"]
        return username, password


if __name__ == '__main__':
    SETTINGS.SET_CREDENTIAL_INTERACTIVE()
    u, p = get_credential()
    print(u, p)
