import os
from pyspark.sql import SparkSession
from pyspark import SparkContext, SparkConf

class LocalSparkConnector:

    @ classmethod
    def getSparkSession(self, master_node : str = 'local', app_name : str = "MySparkConnector"):
        conf = SparkConf().setAppName(app_name)
        spark = SparkSession(SparkContext.getOrCreate(conf = conf))
        #spark = SparkSession.builder.master(master_node).appName(app_name).getOrCreate(conf = conf)
        return spark

    def __init__(self, master_node : str = 'local', app_name : str = "MySparkConnector"):
        self.spark = self.getSparkSession(master_node, app_name)

    def get_file_path(self, path: str) -> str:
        return path

    def get_common_reader(self, header = True, sep = ",", schema = None):
        if schema:
            return self.spark.read.option('header', header) \
                .option("inferSchema" , "false") \
                .option("sep", sep) \
                .schema(schema)
        else:
            return self.spark.read.option('header', header) \
                .option("inferSchema" , "true") \
                .option("sep", sep)


    def read_csv(self, path: str, header = True, sep = ",", schema = None):
        path = self.get_file_path(path)

        try:
            df = self.get_common_reader(header, sep, schema).csv(path)
        except Exception as e:
            print('File Load FAILED: '+ str(e))
        else:
            print('File Load successfully.')
            return df

    def read_parquet(self, path: str, header = True, sep = ",", schema = None):
        path = self.get_file_path(path)

        try:
            df = self.get_common_reader(header, sep, schema).parquet(path)
        except Exception as e:
            print('File Load FAILED: '+ str(e))
        else:
            print('File Load successfully.')
            return df

    def save_csv(self, df, path: str, header = True, sep = ",", mode = "overwrite", repartition: int = None):
        path = self.get_file_path(path)

        if repartition:
            df = df.repartition(repartition)

        try:
            df.write.option('header', header).option("sep", sep).mode(mode).csv(path)
        except Exception as e:
            print('File Wrote FAILED: '+ str(e))
        else:
            print('File Wrote successfully.')

    def save_parquet(self, df, path: str, header = True, sep = ",", mode = "overwrite", repartition: int = None):
        path = self.get_file_path(path)

        if repartition:
            df = df.repartition(repartition)

        try:
            df.write.option('header', header).option("sep", sep).mode(mode).parquet(path)
        except Exception as e:
            print('File Wrote FAILED: '+ str(e))
        else:
            print('File Wrote successfully.')


class SparkMinioConnector(LocalSparkConnector):
    ACCESS_KEY = os.getenv('STS_ACCESSKEY')
    SECRET_ACCESS_KEY = os.getenv('STS_SECRETKEY')
    SESSION_TOKEN = os.getenv('STS_SESSIONTOKEN')
    STORAGE_CERTIFICATE = os.getenv('CACERT')
    STORAGE_URL = os.getenv('STORAGE_URL')

    BUCKET = os.getenv('BUCKET')  # the root bucket in AWS S3/MinIO. by default, is 'cbprice
    FOLDER = os.getenv('BUCKET_FOLDER') # the root folder within the bucket. by default, is 'int'

    @ classmethod
    def getSparkSession(cls, master_node : str = 'local', app_name : str = "MySparkConnector"):
        #spark = SparkSession.builder.master(master_node).appName(app_name).getOrCreate()
        #spark._jsc.hadoopConfiguration().set("fs.s3a.aws.credentials.provider","org.apache.hadoop.fs.s3a.TemporaryAWSCredentialsProvider")
        #spark._jsc.hadoopConfiguration().set("spark.hadoop.fs.s3a.endpoint", cls.STORAGE_URL)
        #spark._jsc.hadoopConfiguration().set("spark.hadoop.fs.s3a.access.key", cls.ACCESS_KEY)
        #spark._jsc.hadoopConfiguration().set("spark.hadoop.fs.s3a.secret.key", cls.SECRET_ACCESS_KEY)
        #spark._jsc.hadoopConfiguration().set("spark.hadoop.fs.s3a.session.token", cls.SESSION_TOKEN)
        #spark._jsc.hadoopConfiguration().set("fs.s3a.impl","org.apache.hadoop.fs.s3a.S3AFileSystem")
        #spark._jsc.hadoopConfiguration().set("spark.hadoop.fs.s3a.fast.upload", "true")
        #spark._jsc.hadoopConfiguration().set("spark.hadoop.fs.s3a.path.style.access", "true")
        #spark._jsc.hadoopConfiguration().set("spark.hadoop.fs.s3a.impl", "org.apache.hadoop.fs.s3a.S3AFileSystem")
        
        conf = SparkConf().setAppName(app_name)
        conf.set("spark.hadoop.fs.s3a.endpoint", cls.STORAGE_URL) \
            .set('spark.hadoop.fs.s3a.aws.credentials.provider', 'org.apache.hadoop.fs.s3a.TemporaryAWSCredentialsProvider') \
            .set('spark.hadoop.fs.s3a.access.key', cls.ACCESS_KEY) \
            .set('spark.hadoop.fs.s3a.secret.key', cls.SECRET_ACCESS_KEY) \
            .set('spark.hadoop.fs.s3a.session.token', cls.SESSION_TOKEN) \
            .set("spark.hadoop.fs.s3a.fast.upload", True) \
            .set("spark.hadoop.fs.s3a.path.style.access", True) \
            .set("spark.hadoop.fs.s3a.impl", "org.apache.hadoop.fs.s3a.S3AFileSystem")

        spark = SparkSession(SparkContext.getOrCreate(conf = conf))
        return spark

    def get_file_path(self, path: str) -> str:
        if path.startswith('/'):
            key = f"s3a://{self.BUCKET}/{self.FOLDER}" + path
        else:
            key = f"s3a://{self.BUCKET}/{self.FOLDER}" + '/' + path
        return key