import json
import os
from collections import OrderedDict
import boto3
import io
import pandas as pd
import yaml
from botocore.exceptions import ClientError

''' ---- 			file loader 			---- '''

def loadJSON(file):
    if isinstance(file, (list, dict)):
        return file
    elif isinstance(file, str):
        with open(file, "rb") as obj:
            return json.load(obj, object_pairs_hook = OrderedDict)

    else:
        raise ValueError("Please parse a file path or JS object")


def toJSON(js, file):
    with open(file, "w") as obj:
        json.dump(js, obj, indent = 4)



'''--- MinIO Accessor ---'''

class MinIoAccessor:
    ACCESS_KEY = os.getenv('STS_ACCESSKEY')
    SECRET_ACCESS_KEY = os.getenv('STS_SECRETKEY')
    SESSION_TOKEN = os.getenv('STS_SESSIONTOKEN')
    STORAGE_CERTIFICATE = os.getenv('CACERT')
    STORAGE_URL = os.getenv('STORAGE_URL')

    BUCKET = os.getenv('BUCKET')  # the root bucket in AWS S3/MinIO. by default, is 'cbprice
    FOLDER = os.getenv('BUCKET_FOLDER') # the root folder within the bucket. by default, is 'int'

    def __init__(self):
        try:
            self.client = boto3.client(
                's3',
                aws_access_key_id = self.ACCESS_KEY,
                aws_secret_access_key = self.SECRET_ACCESS_KEY,
                aws_session_token = self.SESSION_TOKEN,
                verify = self.STORAGE_CERTIFICATE,
                endpoint_url = self.STORAGE_URL
            )

            self.resource = boto3.resource(
                's3',
                aws_access_key_id = self.ACCESS_KEY,
                aws_secret_access_key = self.SECRET_ACCESS_KEY,
                aws_session_token = self.SESSION_TOKEN,
                verify = self.STORAGE_CERTIFICATE,
                endpoint_url = self.STORAGE_URL
            )
        except Exception as e:
            print('Boto3 failed to create client to access AWS S3 / MinIO object: ' + str(e))
            self.client = None
        else:
            print('Boto3 client created successfully.')

    @ classmethod
    def refresh(cls):
        cls.ACCESS_KEY = os.getenv('STS_ACCESSKEY')
        cls.SECRET_ACCESS_KEY = os.getenv('STS_SECRETKEY')
        cls.SESSION_TOKEN = os.getenv('STS_SESSIONTOKEN')
        cls.STORAGE_CERTIFICATE = os.getenv('CACERT')
        cls.STORAGE_URL = os.getenv('STORAGE_URL')

    def list_objs(self, path: str) -> pd.DataFrame:
        if path.startswith('/'):
            prefix = self.FOLDER + path
        else:
            prefix = self.FOLDER + '/' + path

        try:
            res = self.client.list_objects(Bucket = self.BUCKET, Prefix = prefix)
        except ClientError as e:
            print(f'Error occurred when listing objects/files under path: {self.BUCKET}/{prefix}: \n{e}')
        else:
            if 'Contents' not in res.keys():
                print('No objects found at ' + self.BUCKET + '/' + prefix)
            else:
                df = pd.DataFrame.from_records(res['Contents'])

                print('There are: '+ str(len(res['Contents'])) + ' files in ' + self.BUCKET + '/' + prefix)
                return df[['Key', 'LastModified', 'Size']]

    def upload_file(self, local_path: str, remote_path: str):
        '''upload file from local machine to MinIO

        :param local_path: local file path that you would like to upload to MinIO, suggest to be absolute path
        :param remote_path: remote path on MinIO (excluding bucket and root folder); could either start with / or not
        :return:

        example:
        >>> mia = MinIoAccessor()
        >>> mia.upload_file('/home/jovyan/requirements.txt', '/CP/Personal/Allan/requirements.txt')  # start with /CP or CP, don't include bucket name 'int'
        '''
        if remote_path.startswith('/'):
            key = self.FOLDER + remote_path
        else:
            key = self.FOLDER + '/' + remote_path

        # Setting an extremly large multipart threshold to effectively disable multipart uplaod.
        # Note: Multiplart upload is not compatible with GCS, therefore disabling multipart upload
        #       for compatability with hybrid solution.
        config = boto3.s3.transfer.TransferConfig(multipart_threshold = 1000000000000)

        try:
            self.client.upload_file(local_path, self.BUCKET, key, Config = config)
        except Exception as e:
            print('File upload FAILED: '+ str(e))
        else:
            print('File uploaded successfully.')

    def download_file(self, remote_path: str, local_path: str):
        '''download file from MinIO to local machine

        :param remote_path: remote path on MinIO (excluding bucket and root folder); could either start with / or not
        :param local_path: local file path that you would like to save the file from MinIO, suggest to be absolute path
        :return:

        example:
        >>> mia = MinIoAccessor()
        >>> mia.download_file('/CP/Personal/Allan/requirements.txt', '/home/jovyan/requirements.txt')  # start with /CP or CP, don't include bucket name 'int'
        '''
        if remote_path.startswith('/'):
            key = self.FOLDER + remote_path
        else:
            key = self.FOLDER + '/' + remote_path


        try:
            self.client.download_file(self.BUCKET, key, local_path)
        except Exception as e:
            print('File download FAILED: '+ str(e))
        else:
            print('File downloaded successfully.')

    def read_parquet(self, remote_path: str) -> pd.DataFrame:
        if remote_path.startswith('/'):
            key = self.FOLDER + remote_path
        else:
            key = self.FOLDER + '/' + remote_path

        buffer = io.BytesIO()
        s3_object = self.resource.Object(self.BUCKET, key)
        s3_object.download_fileobj(buffer)
        table = pd.read_parquet(buffer)
        return table

    def save_parquet(self, df: pd.DataFrame, remote_path: str):
        if remote_path.startswith('/'):
            key = self.FOLDER + remote_path
        else:
            key = self.FOLDER + '/' + remote_path

        buffer = io.BytesIO()
        df.to_parquet(buffer)
        self.resource.Object(self.BUCKET, key).put(Body = buffer.getvalue())

    def read_csv(self, remote_path: str) -> pd.DataFrame:
        if remote_path.startswith('/'):
            key = self.FOLDER + remote_path
        else:
            key = self.FOLDER + '/' + remote_path

        buffer = io.StringIO()
        s3_object = self.resource.Object(self.BUCKET, key)
        s3_object.download_fileobj(buffer)
        table = pd.read_csv(buffer)
        return table

    def save_csv(self, df: pd.DataFrame, remote_path: str):
        if remote_path.startswith('/'):
            key = self.FOLDER + remote_path
        else:
            key = self.FOLDER + '/' + remote_path

        buffer = io.StringIO()
        df.to_csv(buffer)

        config = boto3.s3.transfer.TransferConfig(multipart_threshold = 8388608000) # 8GB threshold

        self.resource.Object(self.BUCKET, key).put(Body = buffer.getvalue())
