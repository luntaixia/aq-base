import os
from datetime import date

import logging
import pandas as pd

from CommonTools.utils import str2dt

# database connection
from CommonTools.dbapi import ScotiaDB2, dbIO
from CommonTools.login import get_credential

username, PASSWORD = get_credential()

lm_OWSTAR = ScotiaDB2(db = "OWSTAR")  # initiate a Scotia DB2 Conf object
lm_OWSTAR.login(username, PASSWORD)   # login to the database using your SID and password
db_OWSTAR = dbIO(lm_OWSTAR)

lm_DM1P1D = ScotiaDB2(db = "DM1P1D")
lm_DM1P1D.login(username, PASSWORD)   # login to the database using your SID and password
db_DM1P1D = dbIO(lm_DM1P1D)



def get_exchg_rt(start_dt = "2005-01-03", end_dt = "2100-12-31", cur: str = "USD"):
    logging.info(f'Querying exchange rates for {cur}/CAD')

    sql = ''' 
    SELECT 
        MAX(EFF_DT) AS MAX_DATA_DT 
    FROM 
        EDRDS.RPM_RT_FORGN_EXCH_RATES_SNAPSHOT;
    '''
    max_data_dt = db_DM1P1D.query_sql_df(sql).iloc[0,0]
    max_dt = min(str2dt(max_data_dt), str2dt(end_dt))
    min_dt = max(date(2005, 1, 3), str2dt(start_dt))


    sql = ''' 
    SELECT DISTINCT 
        EFF_DT, 
        RTL_RT AS EXCHG_RT 
    FROM 
        EDRDS.RPM_RT_FORGN_EXCH_RATES_SNAPSHOT 
    WHERE 
        FROM_CRNCY_CD = ?
        AND EFF_DT BETWEEN '2015-11-12' AND ?  -- use 2015-11-12 to prevent missing value problem
    ORDER BY 
        EFF_DT;
    '''
    df = db_DM1P1D.query_sql_df(sql, cur, end_dt)
    df.sort_values(by = ['EFF_DT', 'EXCHG_RT'], inplace = True)
    df.drop_duplicates(subset = 'EFF_DT', keep = 'last', inplace = True)
    df.reset_index(drop = True, inplace = True)

    if str2dt(start_dt) < date(2015, 11, 12):
        # extract historical usd to cad exchange rate 2005/1/3 to 2015/11/11
        # data from https://ca.investing.com/currencies/usd-cad-historical-data
        # try:
        #     df_loc = pd.read_csv(old_rates)
        # except FileNotFoundError:
        #     path = os.path.join(os.path.dirname(__file__), old_rates)
        #     df_loc = pd.read_csv(path)
        if cur == 'USD':
            sql = """
            SELECT
                EFF_DT, 
                EXCHG_RT 
            FROM 
                DSS.AQ_FORGN_EXCH_RATES_OLD 
            """
            df_loc = db_OWSTAR.query_sql_df(sql)

            df_loc.EFF_DT = pd.to_datetime(df_loc.EFF_DT).dt.date
            df_loc.sort_values(by = 'EFF_DT', inplace = True)
            df_loc.reset_index(drop = True, inplace = True)
            df = pd.concat([df_loc, df])
        else:
            # fx rate < 2015-11-12 is only available for USD/CAD
            logging.warning("fx rate < 2015-11-12 is only available for USD/CAD")


    df.sort_values(by = 'EFF_DT', inplace = True)
    df.reset_index(drop = True, inplace = True)

    full_dt = pd.DataFrame({'EFF_DT' : pd.Series(pd.date_range(date(2005, 1, 3), max_dt)).dt.date})
    df = pd.merge(full_dt, df, how = "left", on = "EFF_DT")
    df['EXCHG_RT'] = df['EXCHG_RT'].fillna(method = 'ffill')  # fill all missing exchange rate

    return df[(df['EFF_DT'] <= str2dt(end_dt)) & (df['EFF_DT'] >= str2dt(start_dt))]


if __name__ == '__main__':
    df = get_exchg_rt()
    print(df)
    print(df.iloc[-1]['EFF_DT'] - df.iloc[0]['EFF_DT'])
