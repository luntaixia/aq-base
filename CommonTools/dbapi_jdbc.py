import jaydebeapi
import jpype
import os
import logging
from CommonTools.remote import getMyRemote, MyRemote
from CommonTools.utils import download_jdbc_jar


class dfConfJdbc:
    def __init__(self, driver: str):
        self.driver = driver
        self.jar_path = None


    def bindServer(self, ip: str, port: int, db: str):
        self.ip = ip
        self.port = port
        self.db = db

    def login(self, username: str, password: str):
        self.username = username
        self.password = password

    def argTempStr(self):
        raise NotImplementedError("Must implement this method to get different arg placeholder for different database")

    def getConnStr(self) -> str:
        raise NotImplementedError("Must implement this method to get connection string")

    def getConnection(self) -> jaydebeapi.Connection:
        conn = jaydebeapi.connect(
            jclassname = self.driver,
            url = self.getConnStr(),
            driver_args = [self.username, self.password],
            jars = self.jar_path
        )
        return conn

class MySQLJdbc(dfConfJdbc):
    def __init__(self):
        super().__init__("com.mysql.jdbc.Driver")

    def argTempStr(self):
        return "%s"

    def getConnStr(self) -> str:
        return f"jdbc:mysql://{self.ip}:{self.port}/{self.db}"

    def prepare(self, jar_folder):
        jar_file_url = "https://repo1.maven.org/maven2/mysql/mysql-connector-java/8.0.27/mysql-connector-java-8.0.27.jar"
        self.jar_path = download_jdbc_jar(jar_file_url, jar_folder)

class SQLServerJdbc(dfConfJdbc):
    def __init__(self):
        super().__init__("com.microsoft.sqlserver.jdbc.SQLServerDriver")

    def argTempStr(self):
        return "%s"

    def getConnStr(self) -> str:
        return f"jdbc:sqlserver://{self.ip}:{self.port};databaseName={self.db}"

    def prepare(self, jar_folder):
        jar_file_url = "https://repo1.maven.org/maven2/com/microsoft/sqlserver/mssql-jdbc/8.4.0.jre8/mssql-jdbc-8.4.0.jre8.jar"
        self.jar_path = download_jdbc_jar(jar_file_url, jar_folder)


class HiveJdbc(dfConfJdbc):
    def __init__(self):
        super().__init__("org.apache.hive.jdbc.HiveDriver")

    def argTempStr(self):
        return "?"

    def getConnStr(self) -> str:
        return f"jdbc:hive2://{self.ip}:{self.port}/{self.db}"

    def prepare(self, jar_folder):
        jar_file_url = "https://repo1.maven.org/maven2/org/apache/hive/hive-jdbc/3.1.2/hive-jdbc-3.1.2.jar"
        self.jar_path = download_jdbc_jar(jar_file_url, jar_folder)


class PrestoJdbc(dfConfJdbc):
    def __init__(self):
        super().__init__("com.facebook.presto.jdbc.PrestoDriver")

    def argTempStr(self):
        return "?"

    def getConnStr(self) -> str:
        return f"jdbc:presto://{self.ip}:{self.port}/{self.db}"

    def prepare(self, jar_folder):
        jar_file_url = "https://repo1.maven.org/maven2/io/prestosql/presto-jdbc/347/presto-jdbc-347.jar"
        self.jar_path = download_jdbc_jar(jar_file_url, jar_folder)


class ScotiaPrestoJdbc(PrestoJdbc):
    def __init__(self, keystore_path: str = os.getenv("PRESTO_KEYSTORE_PATH"), keystore_passwd: str = os.getenv("PRESTO_KEYSTORE_KEY"), trino_instance: str = os.getenv("PRESTO_INSTANCE")):
        super().__init__()

        self.keystore_path = keystore_path
        self.keystore_passwd = keystore_passwd
        self.trino_instance = trino_instance


    def getConnStr(self) -> str:
        trino_param = {
            "SSL" : "true",
            "SSLKeyStorePassword" : self.keystore_passwd,
            "SSLKeyStorePath" : self.keystore_path,
        }
        param = "&".join(f"{k}={v}" for k, v in trino_param.items())
        return f"jdbc:presto://{self.trino_instance}?{param}"

    def prepare(self, jar_folder):
        super().prepare(jar_folder)

        # start JVM for presto
        args = '-Djava.class.path=%s' % self.jar_path
        jvm_path = jpype.getDefaultJVMPath()
        jpype.startJVM(jvm_path, args)
        logging.info(f"JVM successfully laucnhed!")

class ScotiaHiveJdbc(HiveJdbc):
    def prepare(self, jar_folder, conf_folder):
        if not os.path.exists(jar_folder):
            os.makedirs(jar_folder)
        if not os.path.exists(conf_folder):
            os.makedirs(conf_folder)

        # download required jars from sdpsvrwm0125.scglobal.ad.scotiacapital.com in folder /user/jdbc_current
        server =  MyRemote("sdpsvrwm0125.scglobal.ad.scotiacapital.com", 22, self.username, self.password)
        server.download_folder("/user/jdbc_current", jar_folder)
        self.jar_path = ":".join(os.path.join(jar_folder, name) for name in os.listdir(jar_folder))
        logging.info(f"Jars successfully downloaded to {jar_folder}")

        # download krb5 conf file
        krb5_conf = os.path.join(conf_folder, "krb5.conf")
        server = MyRemote("sdpsvrwm0122.scglobal.ad.scotiacapital.com", 22, self.username, self.password)
        server.download("/etc/krb5.conf", krb5_conf)
        self.krb5_conf = krb5_conf
        logging.info(f"krb5.conf successfully downloaded to {self.krb5_conf}")

        # generate jaas.conf file
        jaas = """
        Client {
        com.sun.security.auth.module.Krb5LoginModule required
        debug=true
        doNotPrompt=true
        useKeyTab=false
        useTicketCache=true
        renewTGT=true
        principal="[SID]@SCGLOBAL.AD.SCOTIACAPITAL.COM";
        };
        """.replace("[SID]", self.username)
        jaas_conf = os.path.join(conf_folder, "jaas.conf")
        with open(jaas_conf, 'w') as obj:
            obj.write(jaas)
        self.jaas_conf = jaas_conf
        logging.info(f"jaas.conf successfully created and saved to {self.jaas_conf}")

        # start JVM
        jvm_params = {
            "-Djava.class.path" : self.jar_path,
            "-Djavax.security.auth.useSubjectCredsOnly" : "false",
            "-Djava.security.krb5.debug" : "true",
            "-Dsun.security.krb5.debug" : "true",
            "-Djava.security.krb5.conf" : self.krb5_conf,
            "-Djava.security.auth.login.config" : self.jaas_conf,
            "-Djava.security.debug" : "all"
        }
        args = [f"{k}={v}" for k, v in jvm_params.items()]
        jvm_path = jpype.getDefaultJVMPath()
        jpype.startJVM(jvm_path, *args)
        logging.info(f"JVM successfully laucnhed!")

    def getConnStr(self) -> str:
        hive_server_list = [
            "172.25.152.14:2181",
            "172.25.152.18:2181",
            "172.25.152.52:2181",
            "172.25.152.13:2181",
            "172.25.152.17:2181"
        ]

        hive_params = {
            "serviceDiscoveryMode" : "zooKeeper",
            "transportMode" : "binary",
            "zooKeeperNamespace" : "hiveserver2",
            "principal" : "hive/_HOST@SCGLOBAL.AD.SCOTIACAPITAL.COM",
            "sasl.qop" : "auth"
        }

        servers = ",".join(hive_server_list)
        params = ";".join(f"{k}={v}" for k, v in hive_params.items())
        return f"jdbc:hive2://{servers}/;{params}"

    def getConnection(self) -> jaydebeapi.Connection:
        conn = jaydebeapi.connect(
            jclassname = self.driver,
            url = self.getConnStr(),
        )
        return conn

if __name__ == '__main__':

    from CommonTools.login import get_credential
    from CommonTools.settings import SETTINGS
    import pandas as pd
    import logging

    logging.basicConfig()
    logging.getLogger().setLevel(logging.INFO)
    SETTINGS.SET_CREDENTIAL_INTERACTIVE()  # set to interactive mode

    sh = ScotiaHiveJdbc()
    username, PASSWORD = get_credential()
    sh.login(username, PASSWORD)
    sh.prepare(jar_folder = "/home/jovyan/jdbc_current", conf_folder = "/home/jovyan/work/EDL/confs_py")

    conn = sh.getConnection()

    sql = """
    select customer_id, monthly_income_amt, last_maintenance_dt from caz_gbci.cust_prf limit 10
    """

    df = pd.read_sql(sql, conn, coerce_float = True)