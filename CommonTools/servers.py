from flask import Flask, redirect, url_for, request, jsonify, make_response
import sys
import traceback
from CommonTools import servers_funcs

app = Flask(__name__)

# @app.route('/success/<name>')
# def success(name):
#     return 'welcome %s' % name
#
# @app.route('/login',methods = ['POST', 'GET'])
# def login():
#     if request.method == 'POST':
#         user = request.form['nm']
#         return redirect(url_for('success',name = user))
#     else:
#         user = request.args.get('nm')
#         return redirect(url_for('success',name = user))

@app.route('/app/<func_name>', methods = ['POST'])
def router(func_name: str):
    if request.content_type in ['application/json']:
        req = request.get_json()
    elif request.content_type in ['multipart/form-data']:
        req = request.form
    func2call = getattr(servers_funcs, func_name)
    #func2call = globals()[func_name]
    print(f"redirect to function: {func_name} @ {func2call}")

    try:
        r = func2call(**req)
        response = jsonify(r)
    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        err_msg = ''.join(t for t in traceback.format_exception(exc_type, exc_value, exc_traceback))
        response = make_response(err_msg, 500)
        response.mimetype = "text/plain"
        return response
    else:
        return response




if __name__ == '__main__':
    app.run(host = '10.56.70.110', port = 9999, debug = True)