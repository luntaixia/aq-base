import logging
import os
import requests
import pandas as pd
from datetime import datetime, date

from CommonTools.settings import SETTINGS


def str2dt(dt_str: str, format = "%Y-%m-%d"):
    if isinstance(dt_str, str):
        return datetime.strptime(dt_str, format).date()
    if isinstance(dt_str, date):
        return dt_str
    if isinstance(dt_str, datetime):
        return dt_str.date()
    else:
        return dt_str

def dt2str(dt: date, format = "%Y-%m-%d"):
    if isinstance(dt, str):
        return dt
    if isinstance(dt, (date, datetime)):
        return dt.strftime(format)
    else:
        return dt

def save_xls(names, list_dfs, xls_path):
    with pd.ExcelWriter(xls_path) as writer:
        for n, df in enumerate(list_dfs):
            print(names[n])
            df.to_excel(writer, names[n], index = False)
        #             df.to_excel(writer,'sheet%s' % n, index=False)
        writer.save()


def read_sql_from_file(file):
    with open(file) as obj:
        sql = obj.read()
    return sql

def complie_sql(sql : str, *args):
    # fill args into sql templates with ?
    arg_num_sql = sql.count("?")
    if len(args) != arg_num_sql:
        raise ValueError(f"SQL temp contains {arg_num_sql} placeholders(?) while only {len(args)} parsed")
    return sql.replace("?", "'{}'").format(*args)

def df_strip(data):
    """
    Remove leading and trailing blank for each column

    Parameters:
    data : DataFrame

    Returns : DataFrame
    """
    for var in data.columns:
        if data[var][data[var].notna()].shape[0] == 0:
            pass
        else:
            if type(data[var][data[var].notna()].iloc[0]) is str:
                data[var] = data[var].str.strip()
    return data



def missing_val_chk(data, var):
    if data[var].isnull().any(axis=None):
        print('There are ' + str(data[var].isnull().sum()) + ' missing values for column ' + var + '.')
        string1 = ','.join(['There are ', str(data[var].isnull().sum()), ' missing values for column ', var, '.'])
        #string1=" "
    else:
        print('Column ' + var + ' has no missing value.')
        string1 = 'Column ' + var + ' has no missing value.'
        #string1=" i"
    return string1


def download_jdbc_jar(jar_file_url, jdbc_driver_folder, jar_file_name: str = None):
    if not os.path.exists(jdbc_driver_folder):
        os.makedirs(jdbc_driver_folder)

    if not jar_file_name:
        jar_file_name = jar_file_url.split('/')[-1]

    jar_path = os.path.join(jdbc_driver_folder, jar_file_name)
    with requests.get(
            jar_file_url,
            proxies = {
                'http': SETTINGS.HTTP_PROXY,
                'https': SETTINGS.HTTPS_PROXY
            }
    ) as response, open(jar_path, 'wb') as out_file:
        out_file.write(response.content)

    logging.info(f"Successfully download JDBC jar from {jar_file_url} and saved to {jar_path}")
    return jar_path


def download_jdbc_jars(jdbc_driver_folder):
    jars = [
        {
            'db_provider' : 'db2',
            'jar_file_url' : 'https://repo1.maven.org/maven2/com/ibm/db2/jcc/db2jcc/db2jcc4/db2jcc-db2jcc4.jar',
            'jar_file_name' : 'db2jcc-db2jcc4.jar'
        },
        {
            'db_provider' : 'mysql',
            'jar_file_url' : 'https://repo1.maven.org/maven2/mysql/mysql-connector-java/8.0.27/mysql-connector-java-8.0.27.jar',
            'jar_file_name' : 'mysql-connector-java.jar'
        },
        {
            'db_provider' : 'sqlite',
            'jar_file_url' : 'https://repo1.maven.org/maven2/org/xerial/sqlite-jdbc/3.36.0.3/sqlite-jdbc-3.36.0.3.jar',
            'jar_file_name' : 'sqlite-jdbc.jar'
        },
        {
            'db_provider' : 'sqlserver',
            'jar_file_url' : 'https://repo1.maven.org/maven2/com/microsoft/sqlserver/mssql-jdbc/8.4.0.jre8/mssql-jdbc-8.4.0.jre8.jar',
            'jar_file_name' : 'mssql-jdbc-8.4.0.jre8.jar'
        },
        {
            'db_provider' : 'oracle',
            'jar_file_url' : 'https://repo1.maven.org/maven2/com/oracle/database/jdbc/ojdbc8/19.3.0.0/ojdbc8-19.3.0.0.jar',
            'jar_file_name' : 'ojdbc.jar'
        },
        {
            'db_provider' : 'presto',
            'jar_file_url' : 'https://repo1.maven.org/maven2/io/prestosql/presto-jdbc/347/presto-jdbc-347.jar',
            'jar_file_name' : 'presto.jar'
        },
        {
            'db_provider' : 'postgresql',
            'jar_file_url' : 'https://repo1.maven.org/maven2/org/postgresql/postgresql/42.2.18/postgresql-42.2.18.jar',
            'jar_file_name' : 'postgresql.jar'
        },
        {
            'db_provider': 'sas',
            'jar_file_url': 'https://repos.spark-packages.org/saurfang/spark-sas7bdat/3.0.0-s_2.12/spark-sas7bdat-3.0.0-s_2.12.jar',
            'jar_file_name': 'spark-sas.jar'
        },
        {
            'db_provider': 'parso',  # use to read sas dataset
            'jar_file_url': 'https://repo1.maven.org/maven2/com/epam/parso/2.0.14/parso-2.0.14.jar',
            'jar_file_name': 'parso.jar'
        }
    ]

    if not os.path.exists(jdbc_driver_folder):
        os.makedirs(jdbc_driver_folder)

    for jar in jars:
        db_provider = jar.get('db_provider')
        jar_file_url = jar.get('jar_file_url')
        jar_file_name = jar.get('jar_file_name')

        download_jdbc_jar(jar_file_url, jdbc_driver_folder, jar_file_name)


def readExcel(file, sheetName: str = None, dtype = "str", na_filter: bool = True):
    if file.endswith(".xlsx") or file.endswith(".xls"):
        try:
            with pd.ExcelFile(file) as reader:
                try:
                    file = pd.read_excel(reader, sheetname = sheetName, engine = "python", encoding = "utf8", dtype = dtype, na_filter = na_filter )
                except:
                    file = pd.read_excel(reader, sheet_name = sheetName, dtype = dtype, na_filter = na_filter)
        except Exception as e:
            logging.critical(e)
            file = pd.DataFrame()
    return file

if __name__ == '__main__':
    dt = date(2021, 1, 4)
    print(dt2str(dt))


