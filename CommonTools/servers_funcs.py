from CommonTools.dbapi import SqlLite, dbIO
import pandas as pd
# from CommonTools.sparker import SparkConnector

# SparkConnector.setup(jdbc_driver_folder = '/rcca/ProdEcon/AQ/jars')

lm = SqlLite("/dss/gic/WeeklySalesData/weekly_sales.db")
d = dbIO(lm)

# sc = SparkConnector(app_name = 'flask_demo')


def stat_sales(start_dt, end_dt):


    sql = """
    select
        count(GIC_ACCT_ID) as NUMBER_OF_ACCTS,  -- Number of Certificates
        sum(BK_BAL_CAD_EQV) as AMOUNT_IN_CAD,  -- originations
        sum(NII) as NII,
        sum(BK_RT_X_BAL) as BK_RT_X_BAL,
        sum(TARGET_RT_X_BAL) as TARGET_RT_X_BAL,
        sum(BDL_X_BAL) as BDL_X_BAL,
        sum(FTP_X_BAL) as FTP_X_BAL,
    
        ACCT_CRNCY as CURRENCY,
        HML_SEGMENT as HML_SEGMENT,  -- customer segment
        TYPE as SALES_TYPE,
        GIC_PRD4 as PRODUCT_LVL2,
        GIC_PRD3 as PRODUCT_LVL1,
        PLAN as PLAN,  -- register plan
        Channel,
        DLVY_CHNL_CD as DLVY_CHNL_CD,  -- delivery channel code
        PRC_TP_CD,  -- Pricing Type
        Type_flag,  -- Pricing type group
        Case_Ctrl,
        WEEK_END_DT as WEEK_END_DT
    from
        AQ_DF_RAW
    where
        WEEK_END_DT between ? and ?
    group by
        ACCT_CRNCY,
        HML_SEGMENT,
        TYPE,
        GIC_PRD4,
        GIC_PRD3,
        PLAN,
        Channel,
        DLVY_CHNL_CD,
        PRC_TP_CD,
        Type_flag,
        Case_Ctrl,
        WEEK_END_DT;
    """
    df = d.query_sql_df(sql, start_dt, end_dt)
    print("Query OK!")
    return df.to_dict(orient = 'records')

def extract_parquet_data(limit):
    df = pd.read_parquet('/dss/gic/WeeklySalesData/spot_balance/spot_balance_2021-11-03.parquet')
    print("Query Complete!")

    return df.head(limit).to_dict(orient = 'records')