
class SETTINGS:
    DB_CREDENTIAL_PATH = '/u/s1460341/keys/credentials-confidential.ini' # default path on Pyserver
    DB_CREDENTIAL_INTERACTIVE = False

    HTTP_PROXY = "http://proxyprd.scotia-capital.com:8080"
    HTTPS_PROXY = "http://proxyprd.scotia-capital.com:8080"

    @ classmethod
    def SET_CREDENTIAL_INTERACTIVE(cls):
        """set to interactive mode, good for JupyterHub

        :return:
        """
        cls.DB_CREDENTIAL_INTERACTIVE = True
        cls.DB_CREDENTIAL_PATH = None

    @ classmethod
    def SET_CREDENTIAL_PATH(cls, path = '/u/s1460341/keys/credentials-confidential.ini'):
        """set to script mode, and specify a credential ini file path

        :param path: should be an .ini configure ration file following the below format:
        :return:

        example credential.ini:
        [Database]
        username = s1460341
        password = xxxxxxx
        """
        cls.DB_CREDENTIAL_INTERACTIVE = False
        cls.DB_CREDENTIAL_PATH = path

    @ classmethod
    def SET_PROXY_SERVER(cls, http_proxy: str, https_proxy: str):
        cls.HTTP_PROXY = http_proxy
        cls.HTTPS_PROXY = https_proxy