import numpy as np
import pandas as pd
from scipy import stats
import statsmodels.api as sm
import statsmodels.formula.api as smf
import statsmodels.regression
from sklearn.base import BaseEstimator, ClassifierMixin, RegressorMixin, clone, is_classifier, is_regressor

# https://www.statsmodels.org/stable/regression.html#
# https://www.statsmodels.org/stable/generated/statsmodels.regression.linear_model.RegressionResults.html#statsmodels.regression.linear_model.RegressionResults
# https://stats.stackexchange.com/questions/16493/difference-between-confidence-intervals-and-prediction-intervals
# https://stats.stackexchange.com/questions/136157/general-mathematics-for-confidence-interval-in-multiple-linear-regression


class StatLinearRegressor(BaseEstimator, ClassifierMixin):
    def __init__(self, features : list = None, formula : str = None, y_label : str = None):
        if formula:
            self.formula = formula
            self.regressors = [c.strip() for c in formula.split("~")[1].split("+")]
            if y_label:
                self.y_label = y_label
            else:
                self.y_label = formula.split("~")[0].strip()
        else:
            self.formula = None
            self.regressors = features
            if y_label:
                self.y_label = y_label
            else:
                self.y_label = 'Y'

    def fit(self, X, y, **fit_params):
        if self.formula:
            if isinstance(X, pd.DataFrame):
                train_df = X[self.regressors]
                train_df[self.y_label] = y
            elif isinstance(X, np.ndarray):
                train_df = pd.DataFrame(np.c_[X, y], columns = self.regressors + [self.y_label])
            else:
                raise TypeError("X only support pandas df and numpy array")
            self.model = smf.ols(formula = self.formula, data = train_df)

        else:
            if isinstance(X, pd.DataFrame):
                X = X[self.regressors].array
            self.model = sm.OLS(y, sm.add_constant(X))

        self.r = self.model.fit()

    def predict(self, X):
        if self.formula:
            if isinstance(X, pd.DataFrame):
                test_df = X[self.regressors]
            elif isinstance(X, np.ndarray):
                test_df = pd.DataFrame(X, columns = self.regressors)
            else:
                raise TypeError("X only support pandas df and numpy array")
            return self.results_.predict(test_df)

        else:
            if isinstance(X, pd.DataFrame):
                X = X[self.regressors].array
            return self.results_.predict(sm.add_constant(X, has_constant = 'add'))

    @property
    def results_(self) -> statsmodels.regression.linear_model.RegressionResults:
        if not hasattr(self, "r"):
            raise AttributeError("Dont have attribute `r`, must fit the model first")
        return self.r

    @ property
    def params(self):
        return self.results_.params

    def getSummary(self):
        return self.results_.summary()

    def t_inv(self, alpha = 0.05):
        return stats.t.ppf(1 - alpha / 2, self.results_.df_resid)

    def predict_detail(self, X, alpha : float = 0.05) -> pd.DataFrame:
        """
        Args:
            X: test X
            alpha: 0.05 is default value, two-tail value

        Returns:
            Explanations:
            dfs = predictions.summary_frame(alpha = alpha)
            # how mean_ci is calculated:
            dfs["mean_ci_lower"] = dfs["mean"] - stats.t.ppf(1 - alpha / 2, results.df_resid) * dfs['mean_se']
            dfs["mean_ci_upper"] = dfs["mean"] + stats.t.ppf(1 - alpha / 2, results.df_resid) * dfs['mean_se']

            # how obs_ci is calculated:
            dfs["obs_ci_lower"] = dfs["mean"] - stats.t.ppf(1 - alpha / 2, results.df_resid) * np.sqrt(dfs['mean_se'] ** 2 + results.mse_resid)
        """
        if self.formula:
            if isinstance(X, pd.DataFrame):
                test_df = X[self.regressors]
            elif isinstance(X, np.ndarray):
                test_df = pd.DataFrame(X, columns=self.regressors)
            else:
                raise TypeError("X only support pandas df and numpy array")

            predictions = self.results_.get_prediction(test_df)

        else:
            if isinstance(X, pd.DataFrame):
                X = X[self.regressors].array
            # has_constant: https://stackoverflow.com/questions/36532529/add-constant-in-statsmodels-not-working
            predictions = self.results_.get_prediction(sm.add_constant(X, has_constant = 'add'))

        summ = predictions.summary_frame(alpha = alpha)
        summ['obs_se'] = np.sqrt(summ['mean_se'] ** 2 + self.results_.mse_resid)
        return summ[['mean', 'mean_se', 'obs_se', 'mean_ci_lower', 'mean_ci_upper', 'obs_ci_lower', 'obs_ci_upper']]